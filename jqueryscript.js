$(document).ready(function () {
    displayTasks();
    $('.add-button').on('click', addTask);
    $('#to-do-input').keypress(function (e) {
        if (e.which === 13) {
            e.preventDefault();
            $('.add-button').click();
        }
    });
});

let todoList = getTasks();

function displayTasks() {
    $('#to-do-List').empty();
    let listItem = $('<ul></ul>');
    for (let i = 0; i < todoList.length; i++) {
        if (todoList[i]['checkstatus'] == '1') {

            let checkstate = " checked ";
            listItem.append('<div  class="task" id="task' + i + '" draggable="true">\
            <li class="lists"><input class="check" id="chk' + i + '" ' + checkstate + ' type="checkbox">\
            <span id="text' + i + '" class="checked-text">' +
                todoList[i]['tododata'] + '</span>\
            <button class="remove" id="' + i + '">Delete</button></li></div>');


        } else {

            let checkstate = " ";
            listItem.append('<div  class="task" id="task' + i + '" draggable="true">\
            <li class="lists"><input class="check" id="chk' + i + '" ' + checkstate + ' type="checkbox">\
            <span id="text' + i + '" class="unchecked-text">' +
                todoList[i]['tododata'] + '</span>\
            <button class="remove" id="' + i + '">Delete</button></li></div>');

        }

        $('#to-do-List').append(listItem);
        // $('#chk'+i).on('click', check);
        // $('#'+i).on('click', remove);

        $("#task"+i).children(".lists").children(".check").on('click',check);
        $("#task"+i).children(".lists").children(".remove").on('click',remove);

    }

    
}

function addTask() {
    let currentTask = {};
    let data = $('#to-do-input').val();
    if (data !== "") {
        currentTask["tododata"] = data;
        currentTask["checkstatus"] = 0;
        todoList.push(currentTask);
        localStorage.setItem('info', JSON.stringify(todoList));
        displayTasks();

    } else {
        alert("Task without a task??");
    }
    $('#to-do-input').val('');
}

function getTasks() {
    let tasks = [];
    let tasksInString = localStorage.getItem('info');
    if (tasksInString !== null) {
        tasks = JSON.parse(tasksInString);
    }
    return tasks;

}

function check() {
    let chkid = $(this).attr('id');
    let id = chkid.replace("chk", "");
    if (this.checked) {
        $(this).next().toggleClass("checked-text");
        todoList[id]["checkstatus"] = 1;
    } else {
        $(this).next().toggleClass("unchecked-text");
        todoList[id]["checkstatus"] = 0;
    }
    localStorage.setItem('info', JSON.stringify(todoList));
    displayTasks();

}

function remove() {
    let id = $(this).attr('id');
    if (todoList[id]['checkstatus'] === 0) {
        const message = confirm("Are you sure you want to skip this task without completing?? ");
        if (message) {
            todoList.splice(id, 1);
        } else {
            alert('good :)');
        }
    } else {
        todoList.splice(id, 1);
    }

    localStorage.setItem('info', JSON.stringify(todoList));

    displayTasks();

    return false;
}